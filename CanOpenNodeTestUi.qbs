import qbs;

Project {
    name: "CanOpenNodeTestUi";

    Product {
        name: (project.namePrefixTools + "canopennodetestui");
        type: "application";
        targetName: (project.targetPrefixTools + "CanOpenNodeTestUi");
        consoleApplication: false;
        cpp.rpaths: ["$ORIGIN", "$ORIGIN/lib"];

        Depends { name: "cpp"; }
        Depends { name: "Qt"; submodules: ["core", "gui", "network", "qml", "quick"]; }
        Depends { name: (project.namePrefixBase + "base"); }
        Depends { name: (project.namePrefixProtocols + "canopen"); }
        Depends { name: (project.namePrefixBase + "utils"); }
        Depends { name: "libqtqmltricks-qtsupermacros"; }
        Depends { name: "libqtqmltricks-qtqmlmodels"; }
        Depends { name: "libqtqmltricks-qtquickuielements"; }
        Group {
            name: "C++ sources";
            files: [
                "main.cpp",
                "SharedObject.cpp",
            ]
        }
        Group {
            name: "C++ header";
            files: [
                "SharedObject.h",
            ]
        }

        Group {
            name: "QML documents";
            files: [
                "ui_CanOpenNodeTestUi.qml",
            ]
        }
        Group {
            name: "Qt resources";
            files: [
                "data.qrc",
            ]
        }
        Group {
            qbs.install: true;
            fileTagsFilter: product.type;
        }
    }
}
